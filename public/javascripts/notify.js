let browserHasNotification = "Notification" in window

let callback = function () {
  if (!browserHasNotification) {
    callback = () => (null)
    console.warn("Notification not supported on browser")
  } else if (Notification.permission === "granted") {
    callback = notify
    return notify(...arguments)
  } else if (Notification.permission !== "denied") {
    Notification.requestPermission(function (permission) {
      if (permission === "granted") {
        callback = notify
        return notify(...arguments)
      } else {
        callback = () => (null)
      }
    })
  }
}

function notify (title, options) {
  return new Notification(title, options)
}

module.exports = function Notify (title, options) {
  return callback(title, options)
}
