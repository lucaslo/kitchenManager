

function newOrder (order) {
  let notification = Notify("New order from: " + order.waiter.name, {
    badge: "images/newMessage.png",
    icon: "images/newMessage.png",
    body: order.name,
    tag: "?",
    vibrate: true,
    renotify: false,
    requireInteraction: false
  })
  console.log("in show, order" + order)
  if (notification) {
    notification.onclick = focusOnTab
    setTimeout(notification.close.bind(notification), timeout)
  }
}

function focusOnTab () {
  window.focus()
  this.cancel()
}



function notify (title, options) {
  return new Notification(title, options)
}

function Notify (title, options) {
  return callback(title, options)
}
