const check = require("./check")

class dinner {
  constructor(name){
    this.name = name
    this.checks = []
    this.closedChecks = []
    console.log("dinner cosntrucor")
  };

  newCheck(table,waiter){
    let newCheck =new check (table,waiter)
    this.checks.push(newCheck)
    return newCheck
    }
  closeCheck(check){
    let newCheck = check
    var position = this.checks.findIndex(function (element) {
      return element.id === newCheck.id
    })
    this.checks[position].closeCheck()
  }

  ordersForKitchen(){
    let checksWithOpenOrders =this.checks.filter(function(check){
      return (check.openOrders !== [])
    })
    if (checksWithOpenOrders === [])
      return false
    else {
    return checksWithOpenOrders.map(function(check){
      return check.openOrders.filter(function(openOrder){
        return (openOrder.status !== "done")
      })
    })
  }
  }

  openChecks(waiter){
    let waiterName=waiter
    return this.checks.filter(function(check){
      return ((check.waiter === waiterName) && (check.closed ===false))
    })

  }

    searchPosition(key){
    let id = key
    var position = this.checks.findIndex(function (element) {
      return element.id === id
    })
    return position
  }

}

module.exports = new dinner()
