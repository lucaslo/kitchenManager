module.exports =  class check{
  constructor(table,waiter){
    this.orders = []
    this.openOrders = []
    this.tableIdentification = table
    this.waiter = waiter
    this.total = 0
    this.orderId = 0
    this.closed = false
    this.id = check.incrementId()
  }

  newOrder(item,amount=1){
    console.log("open order")
    this.orderId++;
    this.total = this.total + item.price*amount
    this.openOrders.push(new order(item,this.orderId,this.id,amount))
  }
  closeCheck(){
    this.closed = true
    this.openOrders= []
  }
  closeOrder(id){
    let orderId = id
    var position = this.openOrders.findIndex(function (element) {
      return element.id === orderId
    })

    if(position > -1){
      console.log("closing order")
      this.openOrders[position].delivered()
      this.orders.push(this.openOrders[position])
      this.openOrders.splice(position,1)
    }else{
      console.log("couldnt close order")
    }


  }
  changeOrderStatus(id){
    let orderId = id
    var position = this.openOrders.findIndex(function (element) {
      return element.id === orderId
    })

    if(position > -1){
      console.log("closing order")
      if(this.openOrders[position].status==="preparing"){
        this.openOrders[position].done()
      }else
        this.openOrders[position].preparing()
    }else{
      console.log("couldnt close order")
    }


  }
  static incrementId() {
    if (!this.latestId) this.latestId = 1
    else this.latestId++
    return this.latestId
  }
}

class order {
  constructor (item,id,checkId,amount=1){
    this.status = "ordered"
    this.item =  item
    this.amount = amount
    this.id = id
    this.checkId = checkId
  }

  preparing(){
    this.status = "preparing"
  }

  done(){
    this.status = "done"
  }

  delivered(){
    this.status = "delivered"
  }
}
