const User = require("../resources/user")

var express = require('express');
var router = express.Router();
const staff = require("../resources/staff")
const menu = require("../resources/menu")
const check = require("../resources/check")
const dinner = require("../resources/dinner")



let tag = this
router.get('/:kitchenName', function (req,res,next){
  let kitchen = req.params.kitchenName
  let orders = dinner.ordersForKitchen()
  if(orders.length===0)
    orders=undefined

  res.render('kitchen',{error:false,user:false,settings:staff.search(kitchen),orders:orders})

})
router.get('/changeOrderStatus/:checkId/:orderId/:kitchenName', function (req,res,next){
  let checkId = parseInt(req.params.checkId,10)
  let orderId= parseInt(req.params.orderId,10)
  let kitchen = req.params.kitchenName

  let position = dinner.searchPosition(checkId)
  dinner.checks[position].changeOrderStatus(orderId)
  let orders = dinner.ordersForKitchen()
  if(orders === [])
    orders=false
  res.render('kitchen',{error:false,user:false,settings:staff.search(kitchen),orders:orders})

})


module.exports = router;
