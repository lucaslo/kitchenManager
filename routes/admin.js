const User = require("../resources/user")
const menu = require("../resources/menu")

var express = require('express');
var router = express.Router();
const staff = require("../resources/staff")

console.log(staff[0])
console.log(staff[1])
let tag = this

router.post('/newMenuItem/:adminName',function(req, res, next){
  let adminName = req.params.adminName
  let adminCredentials = staff.search(adminName)
  console.log(menu.newItem(req.body.itemName,req.body.itemDescription,req.body.itemPrice,req.body.itemSession))
  res.render('menuAdmin',{error:false,menu:menu.show(req.body.itemSession),settings:adminCredentials})
})
router.get("/:adminName",function (req,res,next){
  let adminName = req.params.adminName
  let adminCredentials = staff.search(adminName)
  console.log("adminName ----------- "+ adminName)
  res.render("admin",{error:false,user:false,settings:adminCredentials});

})
router.post('/newEmployee/:adminName',function(req, res, next){
  let adminName = req.params.adminName
  console.log("adminName ----------- "+ adminName)
  let adminCredentials = staff.search(adminName)

  let user = staff.newEmployee(req.body.username,req.body.username,req.body.role,req.body.name,req.body.contact)
  res.render("admin",{error:false,user:false,settings:adminCredentials});
})

router.post('/newMenuItem/:adminName',function(req, res, next){
  let adminName = req.params.adminName
  let adminCredentials = staff.search(adminName)
  console.log(menu.newItem(req.body.itemName,req.body.itemDescription,req.body.itemPrice,req.body.itemSession))
  res.render("admin",{error:false,user:false,settings:adminCredentials});
})


router.post('/searchEmployee/:adminName',function(req, res, next){
  let adminName = req.params.adminName

  let username = req.body.username;
  console.log("adminName ----------- "+ adminName)
  let adminCredentials = staff.search(adminName)

  let waiter = staff.search(username)
  if(waiter){
    res.render('admin',{error:false,user:waiter,settings:adminCredentials})
  }else{
    res.render('admin',{error:"There is no Waiter with that name",user:false,settings:adminCredentials})

  }
})


router.get('/menu/:menuSession/:adminName',function(req, res, next){
  let menuSession= req.params.menuSession
  let adminName = req.params.adminName
  let adminCredentials = staff.search(adminName)

  console.log("menu Session" + menuSession)
  console.log(menu.show(menuSession))
  res.render('menuAdmin',{error:false,menu:menu.show(menuSession),settings:adminCredentials})


})

router.post('/updateEmployee/:adminName',function(req, res, next){
  console.log("found update")

  let adminName=req.params.adminName
  let username = req.body.username;
  let name = req.body.name;
  let contact = req.body.contact;
  let role =  req.body.role
  let waiter = staff.updateEmployee(username,role,name,contact)
  let adminCredentials = staff.search(adminName)

  if(waiter){
    res.render('admin',{error:false,user:waiter,settings:adminCredentials})
  }else{
    res.render('admin',{error:"There is no Waiter with that name",user:false,settings:adminCredentials})

  }
})

module.exports = router;
