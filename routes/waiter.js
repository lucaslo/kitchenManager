const User = require("../resources/user")

var express = require('express');
var router = express.Router();
const staff = require("../resources/staff")
const menu = require("../resources/menu")
const check = require("../resources/check")
const dinner = require("../resources/dinner")



let tag = this
router.get('/:waiter', function (req,res,next){
  let waiterName = req.params.waiter
  let session = staff.search(waiterName)
  console.log("open orders"+ dinner.ordersForKitchen())
  res.render('waiter',{error:false,user:false,settings:session,checks:dinner.openChecks(waiterName)})

})
router.post('/newCheck',function(req, res, next){
  console.log("new Check")
  let table = req.body.table;
  let waiter = req.body.waiter;
  let newCheck = dinner.newCheck(table,waiter)
  res.render('menu',{check:newCheck,menuSession:"all",menu:menu.show("all"),settings:staff.search(waiter)})


  }
)

router.get("/editCheck/:id/:waiter", function(req,res,next){
  let key = parseInt(req.params.id,10)
  let waiter = req.params.waiter
  let check = dinner.openChecks(waiter).find(function (element) {
    return element.id === key
  })
  console.log(check)
  res.render('checkManagement',{check:check,menu:menu,settings:staff.search(waiter)})

})

router.get("/closeCheck/:id/:waiter", function(req,res,next){
  let key = parseInt(req.params.id,10)
  let waiter = req.params.waiter
  let check = dinner.openChecks(waiter).find(function (element) {
    return element.id === key
  })

  dinner.closeCheck(check)

  res.render('waiter',{error:false,user:false,settings:staff.search(waiter),checks:dinner.openChecks(waiter)})

})

router.get("/deliverOrder/:checkId/:orderId", function(req,res,next){
  let key = parseInt(req.params.checkId,10)
  let keyOrder = parseInt(req.params.orderId,10)

  let position = dinner.searchPosition(key)
  console.log("key"+ key+ " position " +position)
  dinner.checks[position].closeOrder(keyOrder)
  res.render('checkManagement',{check:dinner.checks[position],settings:staff.search(dinner.checks[position].waiter)})

})



router.get("/addItem/:checkId/:itemName/:menuSession", function(req,res,next){
  let key = parseInt(req.params.checkId,10)
  let itemName = req.params.itemName
  let menuSession = req.params.menuSession
  console.log("menuSession:" + menuSession)
  let position = dinner.searchPosition(key)
  if(itemName!=="first"){
  dinner.checks[position].newOrder(menu.getItem(itemName))
  }
  res.render('menu',{check:dinner.checks[position],menuSession:menuSession,menu:menu.show(menuSession),settings:staff.search(dinner.checks[position].waiter)})

})
module.exports = router;
