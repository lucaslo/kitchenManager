const User = require("../resources/user")

var express = require('express');
var router = express.Router();
var staff = require("../resources/staff")
let tag = this
router.get('/', function(req, res, next) {
  res.render('index', { error: false });
});

router.post('/login', function(req, res, next) {
  console.log(req.body)
  let username = req.body.username;
  let password = req.body.password;
  let session = staff.search(username)
  if(session){
    if(session.password === password){
      const token = {
        username:username,
        permission:session.role
      }
      staff.punchIn(username)
      if (session.role==="both"){
        res.render('waiter',{token:token,error:false,user:false,settings:session,checks:false,orders:false})

      }else
      res.render(session.role,{token:token,error:false,user:false,settings:session,checks:false,orders:false})
    }else{
      res.render('index',{error:"Invalid Credentials!"});

    }
    }
  else{
  console.log(session);
  res.render('index',{error:"Invalid Credentials!"});
}
});


module.exports = router;
